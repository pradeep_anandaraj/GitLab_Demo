﻿using Microsoft.AspNetCore.Identity;

namespace NTGFreightHawk.Infrastructure.Identity
{
    public class ApplicationUser : IdentityUser
    {
    }
}
