﻿using NTGFreightHawk.Service.Common.Interfaces;
using System;

namespace NTGFreightHawk.Infrastructure.Services
{
    public class DateTimeService : IDateTime
    {
        public DateTime Now => DateTime.Now;
    }
}
