﻿using NTGFreightHawk.Domain.Common;
using NTGFreightHawk.Domain.Entities;

namespace NTGFreightHawk.Domain.Events
{
    public class TodoItemCreatedEvent : DomainEvent
    {
        public TodoItemCreatedEvent(TodoItem item)
        {
            Item = item;
        }

        public TodoItem Item { get; }
    }
}
