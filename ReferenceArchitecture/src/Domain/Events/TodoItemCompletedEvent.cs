﻿using NTGFreightHawk.Domain.Common;
using NTGFreightHawk.Domain.Entities;

namespace NTGFreightHawk.Domain.Events
{
    public class TodoItemCompletedEvent : DomainEvent
    {
        public TodoItemCompletedEvent(TodoItem item)
        {
            Item = item;
        }

        public TodoItem Item { get; }
    }
}
