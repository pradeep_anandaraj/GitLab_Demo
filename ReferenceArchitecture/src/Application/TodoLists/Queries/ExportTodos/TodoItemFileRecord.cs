﻿using NTGFreightHawk.Service.Common.Mappings;
using NTGFreightHawk.Domain.Entities;

namespace NTGFreightHawk.Service.TodoLists.Queries.ExportTodos
{
    public class TodoItemRecord : IMapFrom<TodoItem>
    {
        public string Title { get; set; }

        public bool Done { get; set; }
    }
}
