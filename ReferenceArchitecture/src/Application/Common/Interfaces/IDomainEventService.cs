﻿using NTGFreightHawk.Domain.Common;
using System.Threading.Tasks;

namespace NTGFreightHawk.Service.Common.Interfaces
{
    public interface IDomainEventService
    {
        Task Publish(DomainEvent domainEvent);
    }
}
