﻿using System;

namespace NTGFreightHawk.Service.Common.Interfaces
{
    public interface IDateTime
    {
        DateTime Now { get; }
    }
}
