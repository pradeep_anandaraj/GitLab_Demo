﻿namespace NTGFreightHawk.Service.Common.Interfaces
{
    public interface ICurrentUserService
    {
        string UserId { get; }
    }
}
