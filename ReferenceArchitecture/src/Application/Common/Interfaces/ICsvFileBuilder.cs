﻿using NTGFreightHawk.Service.TodoLists.Queries.ExportTodos;
using System.Collections.Generic;

namespace NTGFreightHawk.Service.Common.Interfaces
{
    public interface ICsvFileBuilder
    {
        byte[] BuildTodoItemsFile(IEnumerable<TodoItemRecord> records);
    }
}
